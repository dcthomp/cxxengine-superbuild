set(cxxengine_extra_optional_dependencies)

set(cxxengine_test_plugin_dir lib)
if (WIN32)
  set(cxxengine_test_plugin_dir bin)
endif()

set(cxxengine_extra_cmake_args)
if (UNIX AND NOT APPLE)
  list(APPEND cxxengine_extra_cmake_args
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif ()

set(cxxengine_rpaths)
if (APPLE)
  list(APPEND cxxengine_rpaths "@loader_path/")
elseif (UNIX)
  list(APPEND cxxengine_rpaths "${superbuild_install_location}/lib")
endif ()

string(REPLACE ";" "${_superbuild_list_separator}"
  cxxengine_rpaths
  "${cxxengine_rpaths}")

set(cxxengine_plugins
  cxxengine-session)
string(REPLACE ";" "${_superbuild_list_separator}"
  cxxengine_plugins
  "${cxxengine_plugins}")

if (POLICY CMP0114)
  set(cxxengine_step_keyword STEP_TARGETS)
else ()
  set(cxxengine_step_keyword INDEPENDENT_STEP_TARGETS)
endif ()

superbuild_add_project(cxxengine
  DEVELOPER_MODE
  DEBUGGABLE
  ${cxxengine_step_keyword} ${cxxengine_lfs_steps} download update
  DEPENDS python3 vtkonly vtk pybind11
  DEPENDS_OPTIONAL ${cxxengine_extra_optional_dependencies} cxx11
  CMAKE_ARGS
    ${cxxengine_extra_cmake_args}
    -DBUILD_TESTING:BOOL=${BUILD_TESTING}
    -Dcxxengine_enable_testing:BOOL=${TEST_cxxengine}

    #specify semi-colon separated paths for session plugins
    -Dcxxengine_test_plugin_paths:STRING=<INSTALL_DIR>/${cxxengine_test_plugin_dir}

    # Set CMAKE_INSTALL_LIBDIR to "lib" for all projects in the superbuild to
    # override OS-specific libdirs that GNUInstallDirs.cmake would otherwise
    # set.
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    # Tell cxxengine where to install the module relative to the superbuild's
    # install tree.
    -Dcxxengine_MODULE_DESTINATION:STRING=lib/python${superbuild_python_version}/site-packages
    # Force VTK's library RPATH into the python module so the install tree's
    # vtkpython will work.
    -DCMAKE_INSTALL_RPATH:STRING=${cxxengine_rpaths}
    # The C++ project's top-level CMakeLists.txt is not in the top of the
    # source tree. Use -S and -B to force the superbuild to use the proper
    # source and build directories relative to the working directory (which
    # for superbuild projects is the build tree next to the source).
    -S ../src/client-server/cxx-engine
    -B .
)

if ((CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang" AND NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "7.0") OR
    (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "3.5"))
 superbuild_append_flags(cxx_flags "-Wno-inconsistent-missing-override" PROJECT_ONLY)
endif ()
