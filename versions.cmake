option(SUPERBUILD_SHALLOW_CLONES "Perform shallow clones rather than full clones" ON)
mark_as_advanced(SUPERBUILD_SHALLOW_CLONES)

superbuild_set_revision(cxxengine
  GIT_REPOSITORY  "https://github.com/Kitware/vtk-web-solutions.git"
  # GIT_TAG         "origin/master"
  GIT_TAG         "origin/foo"
  GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}"
)

superbuild_set_selectable_source(vtkonly
  # VTK v9.0.2
  SELECT release PROMOTE DEFAULT
    URL     "https://www.vtk.org/files/release/9.2/VTK-9.2.6.tar.gz"
    URL_HASH SHA256=06fc8d49c4e56f498c40fcb38a563ed8d4ec31358d0101e8988f0bb4d539dd12
  SELECT git CUSTOMIZABLE
    GIT_REPOSITORY  "https://gitlab.kitware.com/vtk/vtk.git"
    GIT_TAG         "origin/master"
    GIT_SHALLOW     "${SUPERBUILD_SHALLOW_CLONES}"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-vtkonly")

# superbuild_set_revision(vxl
#   GIT_REPOSITORY  "https://gitlab.kitware.com/third-party/vxl.git"
#   GIT_TAG         origin/for/cmb
#   GIT_SHALLOW     "ON")

# superbuild_set_revision(pythonmeshio
#   URL     "https://www.computationalmodelbuilder.org/files/dependencies/meshio-4.3.11.tar.gz"
#   URL_MD5 56593e77540413ceaccaecccd6b55585)
#
# superbuild_set_revision(pythondiskcache
#   URL      "https://www.computationalmodelbuilder.org/files/dependencies/diskcache-3.1.0.tar.gz"
#   URL_HASH SHA256=96cd1be1240257167a090794cce45db02ecf39d20b7a062580299b42107690ac)
#
# superbuild_set_revision(pythonrequests
#   URL     "https://www.computationalmodelbuilder.org/files/dependencies/requests-2.26.0.tar.gz"
#   URL_MD5 8c745949ad3e9ae83d9927fed213db8a)
#
# superbuild_set_revision(pythonrequeststoolbelt
#   URL      "https://www.computationalmodelbuilder.org/files/dependencies/requests-toolbelt-0.8.0.tar.gz"
#   URL_HASH SHA256=f6a531936c6fa4c6cfce1b9c10d5c4f498d16528d2a54a22ca00011205a187b5)
#
# superbuild_set_revision(pythoncharsetnormalizer
#   URL      "https://www.computationalmodelbuilder.org/files/dependencies/charset-normalizer-2.0.6.tar.gz"
#   URL_HASH SHA256=5ec46d183433dcbd0ab716f2d7f29d8dee50505b3fdb40c6b985c7c4f5a3591f)
#
# superbuild_set_revision(pythonidna
#   URL      "https://www.computationalmodelbuilder.org/files/dependencies/idna-3.2.tar.gz"
#   URL_HASH SHA256=467fbad99067910785144ce333826c71fb0e63a425657295239737f7ecd125f3)
#
# superbuild_set_revision(pythonpysocks
#   URL      "https://www.computationalmodelbuilder.org/files/dependencies/PySocks-1.7.1.tar.gz"
#   URL_HASH SHA256=3f8804571ebe159c380ac6de37643bb4685970655d3bba243530d6558b799aa0)
#
# superbuild_set_revision(pythonurllib3
#   URL      "https://www.computationalmodelbuilder.org/files/dependencies/urllib3-1.26.7.tar.gz"
#   URL_HASH SHA256=4987c65554f7a2dbf30c18fd48778ef124af6fab771a377103da0585e2336ece)
#
# superbuild_set_revision(pythoncertifi
#   URL      "https://www.computationalmodelbuilder.org/files/dependencies/certifi-2021.10.8.tar.gz"
#   URL_HASH SHA256=78884e7c1d4b00ce3cea67b44566851c4343c120abd683433ce934a68ea58872)
#
# superbuild_set_revision(pythondocker
#   URL      "https://www.computationalmodelbuilder.org/files/dependencies/docker-5.0.0.tar.gz"
#   URL_MD5 9cc5156a2ff6458a8f52114b9bbc0d7e)
#
# superbuild_set_revision(pythonwebsocketclient
#   URL      "https://www.computationalmodelbuilder.org/files/dependencies/websocket-client-1.1.0.tar.gz"
#   URL_MD5 251ff2f9063dae8d4fca5e7e700d2b7b)

superbuild_set_revision(ftgl
  # https://github.com/ulrichard/ftgl.git
  URL     "https://www.paraview.org/files/dependencies/ftgl-dfd7c9f0dee7f0059d5784f3a71118ae5c0afff4.tar.bz2"
  URL_MD5 16e54c7391f449c942f3f12378db238f)

# # Use json from Wed Mar 20 21:03:30 2019 +0100
# superbuild_set_revision(nlohmannjson
#   URL     "https://www.computationalmodelbuilder.org/files/dependencies/json-295732a81780378c62d1c095078b4634dac8ec28.tar.bz2"
#   URL_MD5 2113211f84a0b01c1c70900285c81e2c)

superbuild_set_revision(pegtl
  # https://github.com/taocpp/PEGTL/releases/tag/2.8.3
  URL "https://data.kitware.com/api/v1/file/64a5ecab93a5dcdba24e0bdd/download/PEGTL-2.8.3.tar.gz"
  URL_MD5 28b3c455d9ec392dd4230402383a8c6f)

# revert common-superbuild update of pybind11, causes macOS test failures.
# will need to update to support python 3.11
superbuild_set_revision(pybind11
  URL     "https://www.paraview.org/files/dependencies/pybind11-2.9.1.tar.gz"
  URL_MD5 7609dcb4e6e18eee9dc1a5f26572ded1)
